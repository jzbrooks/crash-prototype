package com.jzbrooks.crash;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class ServiceActivation extends Activity {
    private TextView textView;
    private Button settingsButton;
    private ServiceReceiver sReciever;
    private ServiceActivation serviceActivation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_activation);

        // Text Field
        textView = (TextView) findViewById(R.id.textFieldTimer);
        // Settings Button
        settingsButton = (Button) findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });

        // Action bar customization
        getActionBar().setBackgroundDrawable(new ColorDrawable(Color.BLUE));

        // Reference to this ptr
        serviceActivation = this;
    }


    @Override
    protected void onResume() {
        super.onResume();
        sReciever = new ServiceReceiver();
        IntentFilter itFilter = new IntentFilter(CrashDetectService.CollectTimer);
        registerReceiver(sReciever, itFilter);

        startService(new Intent(this, CrashDetectService.class));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(sReciever);
        stopService(new Intent(this, CrashDetectService.class));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        /*if (event.getAction() == MotionEvent.ACTION_DOWN) {
            stopService(new Intent(this, CrashDetectService.class));
            startService(new Intent(this, CrashDetectService.class));
        }*/
        return super.onTouchEvent(event);
    }

    public void updateTextView(final String t) {
        ServiceActivation.this.runOnUiThread(new Runnable() {
            public void run() {
                TextView textView = (TextView) findViewById(R.id.textFieldTimer);
                textView.setText(t);
            }
        });
    }

    private class ServiceReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Long timeleft = new Long(intent.getLongExtra("TIMELEFT",-1L));
            if (timeleft == CrashDetectService.RESET_FLAG) {
                updateTextView("Don't Crash!");
            }
            else {
                updateTextView(timeleft.toString());
            }
        }
    };


}
