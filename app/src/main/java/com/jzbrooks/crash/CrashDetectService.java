package com.jzbrooks.crash;

/**
 * Service to detect crashes on a bicycle
 *
 * Author: Justin Brooks
 * Date:   10/22/2014
 *
 */

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class CrashDetectService extends Service implements SensorEventListener {
    public static final String TAG = CrashDetectService.class.getName();
    public static final int SCREEN_OFF_RECEIVER_DELAY = 500;
    public static final String CollectTimer = "TIMER";
    private static final boolean LOGGING_ENABLED = false;
    private static final int TIMER_LENGTH_MILLI = 30000;
    private static final int TIMER_INTERVAL_MILLI = 1000;
    public static final long RESET_FLAG = -5L;

    private int SENSITIVITY;
    private SensorManager mSensorManager = null;
    private WakeLock mWakeLock = null;

    private boolean bFalling = false;
    private boolean bLanded = false;
    private boolean bCountingDown;
    private Intent intent = new Intent(CollectTimer);
    private ToneGenerator tone;

    private CrashDetectService parentService;

    private LocationManager lm;
    private Location location;
    private double mLong;
    private double mLat;

    private CountDownTimer countDownTimer;
    private LocationListener locationListener;

    private boolean mCrashed;
    private boolean timerReset;
    private boolean paused;

    private float[][] accelHistory;
    private int accelHistoryIDX;

    /* Service Overrides */
    @Override
    public void onCreate() {
        super.onCreate();

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        // Setup listening for screen off events
        PowerManager manager =
                (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = manager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
        registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        // ToneGenerator for beeps
        tone = new ToneGenerator(AudioManager.STREAM_ALARM, 90);

        // Location
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mLong = location.getLongitude();
                mLat = location.getLatitude();
            }

            @Override
            public void onStatusChanged(String str, int i, Bundle bundle) {}
            @Override
            public void onProviderEnabled(String str) {}
            @Override
            public void onProviderDisabled(String str) {}
        };

        lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (location != null) {
            mLong = location.getLongitude();
            mLat = location.getLatitude();
        }
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2000, 10, locationListener);

        // set sensitivity from config file
        File file = new File(getApplicationContext().getFilesDir().getAbsolutePath() + "/settings.dat");
        if (file.exists()) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                reader.readLine();
                        reader.readLine();
                reader.readLine();
                String strSens = reader.readLine();
                if (strSens != null)
                    SENSITIVITY = Integer.parseInt(strSens);
                else {
                    SENSITIVITY = 5;
                    Log.d(TAG, "REVERTING TO DEFAULT SENSITIVITY");
                }
            } catch (IOException ex) {
                Log.e(TAG, "ERROR READING FILE!");
            }
        }

        //create countdown timer
        countDownTimer = CreateCountdownTimer();

        accelHistory = new float[10][3];
        accelHistoryIDX = 0;

        parentService = this;
        bCountingDown = false;
        mCrashed = false;
        paused = false;
        timerReset = false;
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mReceiver);
        unregisterListener();
        mWakeLock.release();
        stopForeground(true);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        startForeground(Process.myPid(), new Notification());
        registerListener();
        mWakeLock.acquire();

        return START_STICKY;
    }

    /*
     * Register this as a sensor event listener.
     */
    private void registerListener() {
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST);
    }

    /*
     * Un-register this as a sensor event listener.
     */
    private void unregisterListener() {
        mSensorManager.unregisterListener(this);
    }

    /**
     * Broadcast reciever to listen for screen off event to spin a new thread.
     */
    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (LOGGING_ENABLED) Log.i(TAG, "onReceive("+intent+")");

            if (!intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                return;
            }

            Runnable runnable = new Runnable() {
                public void run() {
                    if (LOGGING_ENABLED) Log.i(TAG, "Runnable executing.");
                    unregisterListener();
                    registerListener();
                }
            };

            new Handler().postDelayed(runnable, SCREEN_OFF_RECEIVER_DELAY);
        }
    };

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        if (LOGGING_ENABLED) Log.i(TAG, "onAccuracyChanged().");
    }

    public void onSensorChanged(SensorEvent event) {
        Float x = event.values[0];
        Float y = event.values[1];
        Float z = event.values[2];

        accelHistory[accelHistoryIDX] = Arrays.copyOf(event.values, 3);

        if (!mCrashed)
            mCrashed = hasCrashed(x, y, z);
        if (mCrashed)
            timerReset = isMovingAfterPotentialCrash(x,y,z);

        if (timerReset) {
            resetService();
            return;
        }

        if (mCrashed)
            handleCrash(30000, 1000);

        accelHistoryIDX = (accelHistoryIDX + 1) % 10;

        if (LOGGING_ENABLED) Log.i(TAG, "onSensorChanged()." + "["+x.toString()+", "+y.toString()+", "+z.toString()+"]");
    }

    /**
     * Helper method to handleCrash situation
     * @param lenInMilli countdown length before messages are sent
     * @param interval interval on the countdown timer
     */
    private void handleCrash(final int lenInMilli, final int interval)
    {
        if (!bCountingDown) {
            bCountingDown = true;
            paused = true;
            countDownTimer.start();
        }
    }

    private void resetService ()
    {
        mCrashed = false;
        bCountingDown = false;
        countDownTimer.cancel();
        //create countdown timer
        countDownTimer = CreateCountdownTimer();
        timerReset = false;
        intent.putExtra("TIMELEFT", RESET_FLAG);
        sendBroadcast(intent);
    }

    private CountDownTimer CreateCountdownTimer()
    {
        return new CountDownTimer(TIMER_LENGTH_MILLI, TIMER_INTERVAL_MILLI) {
            public void onTick(long millisUntilFinished) {
                long seconds = (long) Math.ceil(millisUntilFinished / (double)TIMER_INTERVAL_MILLI % 60);
                intent.putExtra("TIMELEFT", seconds);
                sendBroadcast(intent);
                if (seconds % 4 == 0 && seconds <= 20) {
                    tone.startTone(ToneGenerator.TONE_CDMA_ALERT_CALL_GUARD);
                }
                if (seconds <= 20)
                    paused = false;
            }

            public void onFinish() {
                Toast.makeText(parentService, "Crashed!", Toast.LENGTH_SHORT).show();
                intent.putExtra("TIMELEFT", 0L);
                sendBroadcast(intent);
                File file = new File(getApplicationContext().getFilesDir().getAbsolutePath() + "/settings.dat");
                if (file.exists()) {
                    try {
                        BufferedReader reader = new BufferedReader(new FileReader(file));
                        String temp = "";
                        temp = reader.readLine().trim();
                        if (temp != "")
                            sendSMSMessage(temp);
                        temp = reader.readLine().trim();
                        if (temp != "")
                            sendSMSMessage(temp);
                        temp = reader.readLine().trim();
                        if (temp != "")
                            sendSMSMessage(temp);
                    } catch (IOException ex) {
                        Log.e(TAG, "ERROR READING FILE!");
                    }
                }
            }
        };
    }

    private boolean isMovingAfterPotentialCrash(float x, float y, float z)
    {
        // pause gives time for the crash victim to come to a stop
        return x+y+z > 20 && !paused;
    }

    /**
     * Helper method for determining a crashed state based on input accelerometer data
     * @param x acceleration in x direction
     * @param y acceleration in y direction
     * @param z acceleration in z direction
     * @return true if detects a crashed state (on bicycle) : false otherwise
     */
    private boolean hasCrashed(float x, float y, float z)
    {
        boolean retVal = false;

        int prevIDX = (accelHistoryIDX+1) % 10;
        int numDirChangedRapidly = 0;

        // list of 3D acceleration vectors... check for dramatic change in each
        if (accelHistory[accelHistoryIDX] != null && accelHistory[prevIDX] != null)
        {
            float[] curr = Arrays.copyOf(accelHistory[accelHistoryIDX], 3);
            float[] prev = Arrays.copyOf(accelHistory[prevIDX], 3);
            if (Math.abs(curr[0]-prev[0]) > SENSITIVITY) numDirChangedRapidly++;
            if (Math.abs(curr[1]-prev[1]) > SENSITIVITY) numDirChangedRapidly++;
            if (Math.abs(curr[2]-prev[2]) > SENSITIVITY) numDirChangedRapidly++;
        }

        return numDirChangedRapidly > 2;
    }

    /**
     * Send SMS message with GEO coordinates
     */
    protected void sendSMSMessage(String phoneNo) {
        if (LOGGING_ENABLED) Log.i("Send SMS", "");

        String message = "I have crashed and am not responding. \n\n" + mLat + ", " + mLong;

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, message, null, null);
            Toast.makeText(getApplicationContext(), "Distress SMS sent.",
                    Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(),
                    "SMS failed",
                    Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }
}