package com.jzbrooks.crash;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;


public class SettingsActivity extends Activity {
    private static final String TAG = "SETTINGS ACTIVITY: ";

    private Button saveButton;
    private TextView number1;
    private TextView number2;
    private TextView number3;
    private TextView sens;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        number1 = (TextView) findViewById(R.id.number_one);
        number2 = (TextView) findViewById(R.id.number_two);
        number3 = (TextView) findViewById(R.id.number_three);
        sens = (TextView) findViewById(R.id.sensitivity_in);

        // Check for number already specified
        File file = new File(getApplicationContext().getFilesDir().getAbsolutePath() + "/settings.dat");
        if (file.exists()) {
            try {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                number1.setText(reader.readLine());
                number2.setText(reader.readLine());
                number3.setText(reader.readLine());
                sens.setText(reader.readLine());
            } catch (IOException ex) {
                Log.e(TAG, "ERROR READING FILE!");
            }
        }
        else System.out.println(getApplicationContext().getFilesDir().getAbsolutePath() + "/settings.dat");


        //save button
        saveButton = (Button) findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean success = false;
                File file = new File(getApplicationContext().getFilesDir(), "settings.config");
                try {
                    StringBuilder sb = new StringBuilder();
                    sb.append(number1.getText() + "\n");
                    sb.append(number2.getText() + "\n");
                    sb.append(number3.getText() + "\n");
                    sb.append(sens.getText() + "\n");
                    FileOutputStream fos = new FileOutputStream(file);
                    fos.write(sb.toString().getBytes());
                    success = true;
                } catch(FileNotFoundException ex) {
                    Log.e(TAG, "File not found!");
                } catch(IOException ex) {
                    Log.e(TAG, "IO ERROR!");
                }
                if (success) Toast.makeText(getApplicationContext(), "Saved Settings" , Toast.LENGTH_SHORT).show();
            }
        });
    }

}
